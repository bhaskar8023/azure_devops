export TF_VAR_location="eastus"
export TF_VAR_resource_group_name="gitlab"
export TF_VAR_storage_account_name="gitlab01"
export TF_VAR_container_name="terraform"
export TF_VAR_access_key=${ARM_ACCESS_KEY}
export TF_VAR_client_id=${ARM_CLIENT_ID}
export TF_VAR_client_secret=${ARM_CLIENT_SECRET}
export TF_VAR_subscription_id=${ARM_SUBSCRIPTION_ID}
export TF_VAR_tenant_id=${ARM_TENANT_ID}


#{
 # "appId": "a9202f13-5ae5-49bd-8fea-04cc67efcfea",
 # "displayName": "azure-cli-2022-01-27-22-03-26",
 # "password": "0QCXCayrYhJrTuSTxtBLNOB0HFL-7o9b4s",
 # "tenant": "cb61b85f-e3d0-4f7b-9d96-efa241d19891"
#}

