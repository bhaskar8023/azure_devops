#https://stackoverflow.com/questions/46720266/terraform-unable-to-list-provider-registration-status
provider "azurerm" {
  features {}
}

resource "random_pet" "prefix" {
   length="1"
}

resource "azurerm_resource_group" "default" {
  name     = "${random_pet.prefix.id}-rg"
  location = "East US"

  tags = {
    environment = "aksdev"
  }
}


resource "azurerm_storage_account" "aksstorage" {
  name                     = "${random_pet.prefix.id}sc"
  resource_group_name      = azurerm_resource_group.default.name
  location                 = azurerm_resource_group.default.location
  account_tier             = "Standard"
  account_replication_type = "ZRS"

  tags = {
    environment = "aksdev"
  }
}

resource "azurerm_kubernetes_cluster" "azureaks" {
  name                = "${random_pet.prefix.id}-aks"
  location            = azurerm_resource_group.default.location
  resource_group_name = azurerm_resource_group.default.name
  dns_prefix          = "${random_pet.prefix.id}-k8s"

  default_node_pool {
    name            = "default"
    node_count      = 2
    vm_size         = "Standard_D2_v2"
    os_disk_size_gb = 30
  }

  service_principal {
    client_id     = var.client_id
    client_secret = var.client_secret
  }

  role_based_access_control {
    enabled = true
  }

  tags = {
    environment = "aksdev"
  }
}

