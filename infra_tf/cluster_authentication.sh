mkdir ~/.kube/

touch  ~/.kube/config

#tr -cd '\11\12\15\40-\176' < file-with-binary-chars > clean-file

#echo ${base64encode(terraform output kube_config)} | base64 -d > ~/.kube/config
#make sure to enclose in double quotes"
echo "$(terraform output -raw kube_config)" > ~/.kube/config 

chmod 600 ~/.kube/config

export KUBECONFIG=~/.kube/config
