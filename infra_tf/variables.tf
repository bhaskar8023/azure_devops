variable "client_id" {
  type        = string
  description = "Azure client id"
}

variable "client_secret" {
  type        = string
  description = "Azure client secret"
}

variable "location" {
  type        = string
  description = "loacation of Azure store"
}

variable "resource_group_name" {
  type        = string
  description = "resource group name"
}

variable "storage_account_name" {
  type        = string
  description = "Azure storage account name"
}

variable "container_name" {
  type        = string
  description = "Azure container name"
}

variable "access_key" {
  type        = string
  description = "Azure container access key"
}
