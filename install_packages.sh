apk update && apk search curl bash  && apk add curl bash openssl

#install terraform
wget https://releases.hashicorp.com/terraform/1.1.4/terraform_1.1.4_linux_amd64.zip

unzip terraform_1.1.4_linux_amd64.zip

mv ./terraform /usr/local/bin/terraform

#install kubectl client 
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.18.0/bin/linux/amd64/kubectl
chmod +x ./kubectl
mv ./kubectl /usr/local/bin/kubectl

#install helm package manager
curl -LO https://get.helm.sh/helm-v3.8.0-linux-amd64.tar.gz
tar -zxvf helm-v3.8.0-linux-amd64.tar.gz
mv ./linux-amd64/helm /usr/local/bin/helm
  
#terraform version
#kubectl version --client
#helm help

#curl -sL https://aka.ms/InstallAzureCLIDeb | bash
#Install Azure CLI 
#https://gitanswer.com/install-azure-cli-on-alpine-linux-998886589#:~:text=%20Install%20Azure%20CLI%20on%20Alpine%20Linux%20-,is%20not...%203%20Use%20Dockerfile.%20%20More%20
#apk add py3-pip
#pip install azure-cli
#az version
 # cd to tf and run terraform init
 #  - cd tf && chmod +x ./set_tf_variables.sh && ./set_tf_variables.sh && terraform init && terraform apply && cd ..
#apk cache policy azure-cli
